$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require 'gundoochy'
require 'minitest/autorun'

class GundoochyTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Gundoochy::VERSION
  end

  def test_url
    assert_equal "https://github.com/Sylfrena/Gundoochy", Gundoochy::Url
  end
end
