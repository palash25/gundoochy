require 'gundoochy'
require 'token/token'
require 'lexer/lexer'
require 'minitest'

class LexerTest < Minitest::Test

  def setup
    @lexy = Lexer.new('five = 5;
                       ten = 10;')
  end


  def test_next_token

     tests = {
       IDENT => "five",
       ASSIGN => "=",
       INT => "5",
       SEMICOLON => ";",
       IDENT => "ten",
       ASSIGN => "=",
       INT => "10",
       SEMICOLON => ";",
       #IDENT => "add",
       #ASSIGN => "=",
       #FUNCTION => "def",
       #LPAREN => "(",
       #IDENT => "x",
       #COMMA => ",",
       #IDENT => "y",
       #RPAREN => ")",
       #LCURLY => "{",
       #IDENT => "x",
       #PLUS => "+",
       #IDENT => "y",
       #RCURLY => "}",
       #SEMICOLON => ";",
       #IDENT => "result",
       #ASSIGN => "=",
       #IDENT => "add",
       #LPAREN => "(",
       #IDENT => "five",
       #COMMA => ",",
       #IDENT => "sten",
       #RPAREN => ")",
       #SEMICOLON => ";",
       #EOF => "",

       #PLUS => "+",
       #MINUS => "-",
       #DIVIDE => "/",
       #MULTIPLY => "*",
       #MODULO => "%",
       #EQUAL_TO => "==",
       #GREATER_THAN => ">",
       #LESS_THAN => "<",
       #UNEQUAL_TO => "!=",
       #AND => "&&",
       #OR => "||",
       #NOT => "!",
       #COMMA => ",",
       #SEMICOLON => ";",
       #QUESTION => "?",
       #LPAREN => "(",
       #RPAREN => ")",
       #LSQUARE => "[",
       #RSQUARE => "]",
       #LCURLY => "{",
       #RCURLY => "}"
     }


     tests.each do |expected_type, expected_literal|

        tok = @lexy.next_token
        #puts "moimoi", tok.type, expected_type, tok.literal, expected_literal

        assert_equal expected_type, tok.type

        #puts tok.type, expected_type, tok.literal, expected_literal

        assert_equal expected_literal, tok.literal
     end

  end
end
