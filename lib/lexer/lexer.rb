require_relative '../token/token' #simply require fails to do the job. Is a ruby thing

def is_letter(ch) #basically decides syntax acceptable for variable names
  #puts ch.class
  'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
end

def is_digit(ch) #checks if digit
  '0' <= ch && ch <= '9'
end

class Lexer

  def initialize(input)
    @input = input
    @position = 0
    @readPosition = 0
    @ch =''
    read_char
  end

  def read_char
    #puts caller[0]
    @ch = @readPosition >= @input.length ? '' : @input[@readPosition]
    @position = @readPosition
    @readPosition += 1
    #puts "INSIDE READ_CHAR  #{@position} #{@readPosition} #{@ch}"
  end



  # SUPPOSED TO BE A LOOP WAS JUST A CONDITION. NOW FIXED.
  def consume_whitespace
    while @ch == ' ' || @ch =='\t' || @ch == '\n' || @ch == '\r' do
      read_char
    end
  end

  def read_identifier
    pos = @position
    #puts "RI: char #{@ch}  pos #{pos} position #{@position}"
    while is_letter(@ch) do
      #puts @ch
      read_char
    end
    puts "METHOD read_identifier: char #{@ch}  pos #{pos} position #{@position}\n"
    @input[pos..@position-1]

  end

  def read_number
    pos = @position
    #puts "RN: char #{@ch}  pos #{pos} position #{@position}"
    while is_digit(@ch) do
      read_char
    end
    puts "METHOD read_number: char #{@ch}  pos #{pos} position #{@position}\n"
    @input[pos..@position-1]

  end


  def next_token
    #puts @ch, @ch.class
    #puts "\nX=X=X=X=X=X=X=X=X=:  #{@ch}, #{@ch.ord},  X=X=X=X=X=X=X=X=X=\n"
    tok = nil
    consume_whitespace

    tok =
      case @ch
      when '=' then Token.new(ASSIGN, @ch)
      when '+' then Token.new(PLUS, @ch)
      when '-' then Token.new(MINUS, @ch)
      when '/' then Token.new(DIVIDE, @ch)
      when '*' then Token.new(MULTIPLY, @ch)
      when '%' then Token.new(MODULO, @ch)
      #when '==' then Token.new(EQUAL_TO, @ch)
      when '>' then Token.new(GREATER_THAN, @ch)
      when '<' then Token.new(LESS_THAN, @ch)
      #when '!=' then Token.new(UNEQUAL_TO, @ch)
      #when '&&' then Token.new(AND, @ch)
      #when '||' then Token.new(OR, @ch)
      when '!' then Token.new(NOT, @ch)
      when ',' then Token.new(COMMA, @ch)
      when ';' then Token.new(SEMICOLON, @ch)
      when '?' then Token.new(QUESTION, @ch)
      when '(' then Token.new(LPAREN, @ch)
      when ')' then Token.new(RPAREN, @ch)
      when '[' then Token.new(LSQUARE, @ch)
      when ']' then Token.new(RSQUARE, @ch)
      when '{' then Token.new(LCURLY, @ch)
      when '}' then Token.new(RCURLY, @ch)
      else
        #puts 'hello from next_token', @ch.ord
        # STATE WAS BEING MUTATED NOW FIXED
        puts "letter #{@ch}"
        puts "letter ascii   #{@ch.ord}"
        #puts "isletter  "
        if is_letter(@ch)
          literal = read_identifier
          Token.new(look_up_ident(literal), literal)
        elsif is_digit(@ch)
          Token.new(INT, read_number)
        else
          Token.new(ILLEGAL, "ILLEGAL")
        end
      end
    read_char
    return tok
  end

end
