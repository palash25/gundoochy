class Token

  attr_reader :type #getter methods
  attr_reader :literal

  def initialize(type, literal)
    @type = type
    @literal = literal
  end

end

  ILLEGAL = "ILLEGAL"
  EOF = "EOF"

  # Identifiers
  IDENT = "IDENTIFIER"
  INT = "INT"
  FLOAT = "FLOAT"

  # Operators
  ASSIGN = "="
  PLUS = "+"
  MINUS = "-"
  DIVIDE = "/"
  MULTIPLY = "*"
  MODULO = "%"
  EQUAL_TO = "=="
  GREATER_THAN = ">"
  LESS_THAN = "<"
  UNEQUAL_TO = "!="
  AND = "&&"
  OR = "||"
  NOT = "!"

  # Delimiters
  COMMA = ","
  SEMICOLON = ";"
  QUESTION = "?"

  LPAREN = "("
  RPAREN = ")"
  LSQUARE = "["
  RSQUARE = "]"
  LCURLY = "{"
  RCURLY = "}"

  #Keywords
  FUNCTION = "FUNCTION"
  #SAY = "SAY"


 # keywords = { "def": FUNCTION}

  def look_up_ident(ident)
    #puts "INSIDE LOOK UP: #{ident}"
    keywords = {"def": FUNCTION}
    #puts IDENT
    keywords[ident] ? keywords[ident]: IDENT
    #puts "iafterwork"
  end
